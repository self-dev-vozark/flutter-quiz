import 'package:flutter/material.dart';

import './quiz.dart';
import './result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  var _questionIndex = 0;
  var _totalScore = 0;
  final _questions = const [
    {
      'question': 'What\'s your fav color?',
      'answers': [
        {'text': 'Blue', 'score': 10},
        {'text': 'Red', 'score': 20},
        {'text': 'Purple', 'score': 30},
      ]
    },
    {
      'question': 'What\'s your fav food?',
      'answers': [
        {'text': 'Sate Ayam', 'score': 10},
        {'text': 'Lodeh Terong', 'score': -10},
        {'text': 'Sayur Sop', 'score': 100},
      ]
    }
  ];

  void _answerQuestion(int score) {
    setState(() {
      _questionIndex += 1;
      _totalScore += score;
      print(_questionIndex);
    });
  }

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: Text('Pak bambang tuku gendang'),
            ),
            body: _questionIndex < _questions.length
                ? Quiz(
                    questions: _questions,
                    questionIndex: _questionIndex,
                    callback: _answerQuestion)
                : Result(_totalScore, _resetQuiz)));
  }
}
