import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int finalScore;
  final Function resetFunction;

  Result(this.finalScore, this.resetFunction);

  String get resultPhrase {
    var resultText = 'You dit it!';
    if (finalScore < 20) {
      resultText += ' You are awesome and innocent';
    } else if (finalScore < 50) {
      resultText += ' You are magnificent';
    } else if (finalScore < 100) {
      resultText += ' You are sick!!';
    } else {
      resultText += ' Crazy bij';
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      children: <Widget>[
        Text(resultPhrase,
            style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center),
        FlatButton(
          onPressed: resetFunction,
          child: Text('Reset Bos'),
          textColor: Colors.limeAccent,
          color: Colors.amber,
        ),
        ElevatedButton(
          onPressed: resetFunction,
          child: Text('Reset quiz!'),
          style: ElevatedButton.styleFrom(
              primary: Colors.orange, onPrimary: Colors.white),
        )
      ],
    ));
  }
}
